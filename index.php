<?php

require_once ('Animal.php');
require_once ('Frog.php');
require_once('Ape.php');


$sheep = new Animal("Shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$katak = new Frog('Buduk');
echo "Name : " . $katak->name . "<br>";
echo "Legs : " . $katak->legs . "<br>";
echo "Cold Blooded : " . $katak->cold_blooded . "<br>";
echo "Jump : ";
echo $katak->jump();
echo "<br><br>";

$sungkong = new Ape('Kera Sakit');
echo "Name : " . $sungkong->name . "<br>";
echo "Legs : " . $sungkong->legs . "<br>";
echo "Cold Blooded : " . $sungkong->cold_blooded . "<br>";
echo "Yell : ";
echo $sungkong->yell();
echo "<br>";

?>